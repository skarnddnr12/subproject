package com.kh.subproject.member.service.impl;

import com.kh.subproject.member.repoistiory.MemberRepository;
import com.kh.subproject.member.service.MemberService;
import com.kh.subproject.member.vo.Member;
import com.kh.subproject.member.vo.QMember;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class MemberServiceImpl implements MemberService {

    @Autowired
    MemberRepository memberRepository;

    @Override
    public String joinMember(Member member) {
        member.setAdmin("N");
        member.setStatus("Y");
        Member joinMember = memberRepository.save(member);
        String loginFlag = joinMember.getMid().equals(member.getMid()) ? "success" : "fail";
        return loginFlag;
    }

    @Override
    public Member loginMember(Member member) {
        Predicate predicate = QMember.member.mid.eq(member.getMid())
                             .and(QMember.member.password.eq(member.getPassword()));

        Optional<Member> selectMember = memberRepository.findOne(predicate);

        if(selectMember.isPresent()){
            Member loginMember = selectMember.get();
            if(loginMember.getMid().equals(member.getMid()) && loginMember.getPassword().equals(member.getPassword())){
                return loginMember;
            }
        }
            return null;
    }

    @Override
    public Member selectMember(Member member) {
        return memberRepository.findById(member.getMno()).get();
    }

    @Override
    public Member modifyMember(Member member) {
        Optional<Member> modifyMember = memberRepository.findById(member.getMno());
        if(modifyMember.isPresent()) {
            modifyMember.get().setMid(member.getMid());
            modifyMember.get().setName(member.getName());
            modifyMember.get().setEmail(member.getEmail());
            return modifyMember.get();
        }
            return null;
    }
}
