package com.kh.subproject.board.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.kh.subproject.board.vo.Article;
import com.kh.subproject.board.vo.Board;

public interface BoardRepositoryService {

	Board addBoard(Board board);

	Page<Board> selectBoardList(Pageable pageable);

	void deleteBoard(Board board);

	Board boardUpdate(Board board);

}
