<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org" lang="en">
<title>join</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://kit.fontawesome.com/01ddc534cc.js" crossorigin="anonymous"></script>
<style>
    body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
    .w3-third img{margin-bottom: -6px; opacity: 0.8; cursor: pointer}
    .w3-third img:hover{opacity: 1}
    i{
        color:black;
        font-size:30px;
    }
    .w3-button:hover{
        color:white !important;
    }
</style>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.0"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<body class="w3-light-grey w3-content" style="max-width:1600px">

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-bar-block w3-white w3-animate-left w3-text-grey w3-collapse w3-top w3-center" style="z-index:3;width:300px;font-weight:bold" id="mySidebar"><br>
    <h3 class="w3-padding-64 w3-center"><b>SOME<br>NAME</b></h3>
    <a href="javascript:void(0)" onclick="w3_close()" class="w3-bar-item w3-button w3-padding w3-hide-large">CLOSE</a>
    <a href="/board/boardMain" onclick="w3_close()" class="w3-bar-item w3-button">BOARD</a>
    <a href="#about" onclick="w3_close()" class="w3-bar-item w3-button">MESSAGE</a>
    <c:if test="${sessionScope.loginMember == null}">
        <a href="${pageContext.request.contextPath}/main" onclick="w3_close()" class="w3-bar-item w3-button">LOGIN</a>
    </c:if>
    <c:if test="${sessionScope.loginMember != null}">
        <a href="${pageContext.request.contextPath}/member/logout" onclick="w3_close()" class="w3-bar-item w3-button">LOGOUT</a>
    </c:if>
</nav>

<!-- Top menu on small screens -->
<header class="w3-container w3-top w3-hide-large w3-white w3-xlarge w3-padding-16">
    <span class="w3-left w3-padding">SOME NAME</span>
    <a href="javascript:void(0)" class="w3-right w3-button w3-white" onclick="w3_open()">☰</a>
</header>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px; ">

    <!-- Push down content on small screens -->
    <div class="w3-hide-large" style="margin-top:83px"></div>

    <!-- Photo grid -->
    <div class="w3-row w3-container joinArea" style="min-height:870px;">
        <div class="w3-center" style="margin-top:30px;"><h1>회원정보</h1></div>
        <div class="table-section w3-padding-32" style="min-height:700px">
            <div class="w3-container w3-card-4 w3-light-grey w3-text-blue w3-margin">
                <h2 class="w3-center" style="color:black;">Join Us</h2>

                <div class="w3-row w3-section">
                    <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"></i></div>
                    <div class="w3-rest">
                        <input class="w3-input w3-border" name="mid" type="text" placeholder="ID">
                    </div>
                </div>

                <div class="w3-row w3-section">
                    <div class="w3-col" style="width:50px"><i class="fas fa-user-friends"></i></div>
                    <div class="w3-rest">
                        <input class="w3-input w3-border" name="name" type="text" placeholder="name">
                    </div>
                </div>

                <div class="w3-row w3-section">
                    <div class="w3-col" style="width:50px"><i class="fas fa-envelope"></i></div>
                    <div class="w3-rest">
                        <input class="w3-input w3-border" name="email" type="text" placeholder="Email">
                    </div>
                </div>

                <p class="w3-center">
                    <button class="w3-button w3-section w3-blue w3-ripple" style="background: black !important;" onclick="modifyData();"> modify </button>
                </p>
            </div>
        </div>
    </div>
    <div class="w3-black w3-center w3-padding-24">Powered by <a href="https://www.w3schools.com/w3css/default.asp" title="W3.CSS" target="_blank" class="w3-hover-opacity">w3.css</a></div>

    <!-- End page content -->
</div>

<script>
    $(function(){
       getUserDate();

    });

    function getUserDate(){
        $.ajax({
            url:"/member/select/" + ${sessionScope.loginMember.mno},
            type:"get",
            dataType:"json",
            success:function (data) {
                bindingMember(data);
                readType(true);
            },
            error:function (status) {
                console.log(status);
            }
        })
    }

    function bindingMember(data){
        document.getElementsByName("mid")[0].value = data.mid;
        document.getElementsByName("name")[0].value = data.name;
        document.getElementsByName("email")[0].value = data.email;
    }

    function readType(flag){
        var inputArr = document.getElementsByTagName("input");
        if(flag){
            $.each(inputArr,function(){
                this.setAttribute("readonly","readonly");
                this.style.backgroundColor = "lightgray";
            })
        }else{
            $.each(inputArr,function(){
                this.removeAttribute("readonly");
                this.style.backgroundColor = "white";
            })
        }

    }

    function modifyData(){
        if(document.getElementsByTagName("input")[0].getAttribute("readonly") == "readonly"){
            readType(false);
        }else{
            var modifyData = {
                                mno:${sessionScope.loginMember.mno},
                                mid:document.getElementsByName("mid")[0].value,
                                name:document.getElementsByName("name")[0].value,
                                email:document.getElementsByName("email")[0].value
            }
            $.ajax({
                    url:"/member/modify",
                    type:"put",
                    dataType:"json",
                    contentType:"application/json; charset=utf-8",
                    data:JSON.stringify(modifyData),
                    success:function (data) {
                        console.log(data);
                        readType(true);
                        bindingMember(data);
                    },
                    error:function (data) {
                        console.log(data);
                    }
            })
        }
    }

</script>
</body>
</html>